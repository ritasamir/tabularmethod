import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;


public class tabularMethod {

	private JFrame frame;
	  private JButton button;
	 private JTextField tfield;
	    private String nameTField;
	    private int count;
	    
	    
	    public tabularMethod()
	    {
	        nameTField = "tField";
	        count = 0;
	    }
	    private void displayGUI()
	    {
	        frame = new JFrame("JFrame Example");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.setLayout(new GridLayout(0, 1, 2, 2));
	        button = new JButton("Add JTextField");
	        button.addActionListener(new ActionListener()
	        {
	        	 @Override
	             public void actionPerformed(ActionEvent ae)
	             {
	                 tfield = new JTextField();
	                 tfield.setName(nameTField + count);
	                 count++;
	                 frame.add(tfield);
	                 frame.revalidate();  // For JDK 1.7 or above.
	                 //frame.getContentPane().revalidate(); // For JDK 1.6 or below.
	                 frame.repaint();
	             }
	         });
	         frame.add(button);
	         frame.pack();
	         frame.setLocationByPlatform(true);
	         frame.setVisible(true);
	     }
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new tabularMethod().displayGUI();
            }
        });
	}

	/**
	 * Create the application.
	 */
	
	/**
	 * Initialize the contents of the frame.
	 */
/*	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int num1,num2,ans;
				try{
					num1=Integer.parseInt(textField.getText());
					num2=Integer.parseInt(textField_1.getText());
					ans=num1+num2;
					textField_2.setText("hello"+Integer.toString(ans));
					
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "please insert invalid number");
				}
			
			}
		});
		btnNewButton.setBounds(162, 116, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(22, 24, 200, 50);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(228, 24, 200, 50);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(204, 184, 200, 50);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblResult = new JLabel("result");
		lblResult.setHorizontalAlignment(SwingConstants.CENTER);
		lblResult.setToolTipText("");
		lblResult.setBounds(66, 184, 98, 50);
		frame.getContentPane().add(lblResult);
	}*/
}
