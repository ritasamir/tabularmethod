package tabularMethod;

import java.util.Arrays;
import java.util.Scanner;

public class Test {
	public static void main(final String[] args) {
		// System.out.println(Integer.toBinaryString(11));
		// System.out.println(count(Integer.toBinaryString(7)));
		Scanner scan = new Scanner(System.in);
		int[] array1 = new int[1025];
		int[] array2 = new int[1025];

		int minterm;
		int dontcare;
		int nomofvariable;
		int maxminterm = 0;
		int size1, size2 = 0;
		size1 = 0;
		minterm = scan.nextInt();
		while (minterm != -1) {
			array1[size1] = minterm;
			minterm = scan.nextInt();
			size1++;
		}
		dontcare = scan.nextInt();
		while (dontcare != -1) {
			array2[size2] = dontcare;
			dontcare = scan.nextInt();
			size2++;
		}
		scan.close();

		for (int i = 0; i < size1; i++) {
			if (maxminterm < array1[i]) {
				maxminterm = array1[i];
			}
		}
		for (int i = 0; i < size2; i++) {
			if (maxminterm < array2[i]) {
				maxminterm = array2[i];
			}
		}
		nomofvariable = (int) Math.ceil((Math.log(maxminterm) / Math.log(2)));
System.out.println("nomofvariable="+nomofvariable);
		int[] array3 = new int[size1 + size2];
		System.arraycopy(array1, 0, array3, 0, size1);
		System.arraycopy(array2, 0, array3, size1, size2);

		String[][][] nom1 = new String[nomofvariable + 1][nomofvariable + 1][(int) Math.pow(2, nomofvariable)];

		for (int i = 0; i < array3.length; i++) {
			nom1[0][count(Integer.toBinaryString(array3[i]))][i] = "f" + Integer.toString(array3[i]) + ",";

		}
		int n = 0;
		int number1;
		int number2;
		String string1;
		String string2;
		for (int i = 0; i < nom1.length; i++) {
			n = 0;
			for (int j = 0; j < nom1[0].length - 1; j++) {
				n = 0;
				for (int k = 0; k < nom1[0][0].length; k++) {
					if (nom1[i][j][k] != null) {
						// check1=nom1[i][j][k].charAt(0);
						number1 = Integer.parseInt(nom1[i][j][k].substring(1, nom1[i][j][k].indexOf(',')));
						string1 = nom1[i][j][k].substring(nom1[i][j][k].indexOf(',') + 1);
						//System.out.println("number1=" + number1);
						//System.out.println("string1=" + string1);
						for (int l = 0; l < nom1[0][0].length; l++) {
							if (nom1[i][j + 1][l] != null) {
								number2 = Integer
										.parseInt(nom1[i][j + 1][l].substring(1, nom1[i][j + 1][l].indexOf(',')));
								string2 = nom1[i][j + 1][l].substring(nom1[i][j + 1][l].indexOf(',') + 1);
								// System.out.println("l=" + l);
								//System.out.println("number2=" + number2);
								//System.out.println("string2=" + string2);
								if (number1 < number2 && (isInt(Math.log(number2 - number1) / Math.log(2)))
										&& string1.equals(string2)) {
									//System.out.println("da7'al");
									// System.out.println("n" + n);
									nom1[i + 1][j][n] = "f" + Integer.toString(number1) + "," + string1
											+ Integer.toString(number2 - number1) + ",";
									n++;
									nom1[i][j][k] = nom1[i][j][k].replaceFirst("f", "t");
									nom1[i][j + 1][l] = nom1[i][j + 1][l].replaceFirst("f", "t");
								}
							}

						}
					}
				}
			}
		}

//		for (int i = 0; i < nom1.length; i++) {
//
//			for (int j = 0; j < nom1[0].length; j++) {
//				for (int z = 0; z < nom1[0][0].length; z++) {
//					System.out.println("[" + i + "][" + j + "][" + z + "]:" + nom1[i][j][z]);
//				}
//
//			}
//		}

		String[] primeImplicants  = getPIs(nom1, nomofvariable);
		/*System.out.println("PIs:");
		for(int i = 0; i < primeImplicants.length; i++) {
			System.out.println(primeImplicants[i]);
		}*/

		String[][] PIChart = new String[primeImplicants.length + 1][size1 + 1];
		PIChart[0][0] = "s";
		for (int i = 1; i < PIChart[0].length; i++) {
			PIChart[0][i] = Integer.toString(array1[i - 1]);
		}
		for (int i = 1; i < PIChart.length; i++) {
			PIChart[i][0] = sub(primeImplicants[i - 1]);
		}

		for (int j = 1; j < PIChart.length; j++) {
			String[] splited = split(PIChart[j][0]);
			for (int k = 0; k < splited.length; k++) {
				String dummy = splited[k];
				for (int i = 1; i < PIChart[0].length; i++) {
					if (PIChart[0][i].equals(dummy)) {
						PIChart[j][i] = "x";
					} else if (PIChart[j][i] == null) {
						PIChart[j][i] = "s";
					}

				}
			}
		}
		
		
		String[] essential = new String[primeImplicants.length];
		int counter = 0;
		int o = 0;
		int index = 0;
		for (int j = 1; j < PIChart[0].length; j++) {
			for (int i = 1; i < PIChart.length; i++) {
				if (PIChart[i][j] == "x") {
					index = i;
					counter++;
				}
			}
			if (counter == 1) {
				essential[o] = PIChart[index][0];
				o++;
				for (int k = 1; k < PIChart[0].length; k++) {
					if (PIChart[index][k] == "x") {
						for (int l = 1; l < PIChart.length; l++) {
							PIChart[l][k] = "s";
						}
					}
				}
				for (int k = 1; k < PIChart[0].length; k++) {
					PIChart[index][k] = "s";
				}
				// System.out.println(primeImplicants[x]);
				
			}
			counter = 0;
		}
		
		essential = Arrays.copyOf(essential, o);
	/*	System.out.println("essential");
		for (int i = 0; i < essential.length; i++) {
			System.out.println(essential[i]);
		}*/
		boolean ber=true;
		for (int i = 1; i < PIChart.length; i++) {
		//	System.out.println(ber);
			for (int j = 1; j < PIChart[0].length; j++) {
				if(PIChart[i][j].equals("x")){
					ber=false;
					break;
				}
			}
			
		}
		
		if(ber) {
		//	System.out.println("ana hena");
			printResult(null,essential,nomofvariable);
			return ;
		}
	/*for (int i = 0; i < PIChart.length; i++) {
			for (int j = 0; j < PIChart[0].length; j++) {
				System.out.print(PIChart[i][j] + " ");
			}
			System.out.println();
		}
*/
		String[] petrick = new String[size1];
		for (int i = 0; i < petrick.length; i++) {
			petrick[i] = "";
		}
		int c = 0;
		for (int i = 1; i < PIChart[0].length; i++) {
			for (int j = 1; j < PIChart.length; j++) {
				if (PIChart[j][i].equals("x")) {
					petrick[c] += (char) (64 + j) + ",";
				}
			}
			
			if (!petrick[c].equals("")) {
				 
				c++;
			}
		}
		
		petrick = Arrays.copyOf(petrick, c);
//		String[] petricks = new String[c];
	for (int j = 0; j < petrick.length; j++) {
	
			 System.out.println(petrick[j]);
		}
		
		String[] finalPetrick;
		finalPetrick = multiplyingPIs(petrick);
		//System.out.println(finalPetrick[0]+"****");
		String[] d = finalPetrick[0].split(",");

		// d=sorting(d);
		for (int i = 0; i < d.length; i++) {
			System.out.println("***"+d[i]);
		}
		d = deleteDuplicate(d);
		

		for (int i = 0; i < d.length; i++) {
			System.out.println("*"+d[i]);
		}
		d = minimumSolutions(d);
		int m = d[0].length();
		String[][] result = new String[d.length][m];
		for(int i = 0; i < d.length; i++) {
			String solution = d[i];
			for(int j = 0; j <solution.length(); j++) {
				result[i][j] = getTerm(stringToBinary(nomofvariable,sub(eachPI(solution.charAt(j), primeImplicants))));
			}
		}
		printResult(result,essential,nomofvariable);
		for(int i = 0; i < result.length; i++) {
			for(int j = 0; j < result[0].length; j++) {
				System.out.print(result[i][j] + " ");
			}
			System.out.println();
		}
//		System.out.println("primeImplicants");
//		for (int i = 0; i < primeImplicants.length; i++) {
//			System.out.println(primeImplicants[i]);
//		}
 System.out.println("essential");
		for (int i = 0; i < essential.length; i++) {
			System.out.println(essential[i]);
		}



	}

	public static int count(final String string) {
		int count = 0;
		for (int i = 0; i < string.length(); i++) {
			if (Character.getNumericValue(string.charAt(i)) == 1) {
				count++;
			}
		}
		return count;

	}

	public static int[][][] form(final int[][][] nom1) {
		int j;
		for (int i = 0; i < nom1[0].length; i++) {
			j = 0;
			while (nom1[0][i][0] == -1 && j < nom1[0][0].length) {
				for (int k = 0; k < nom1[0][0].length - 1; k++) {
					nom1[0][i][k] = nom1[0][i][k + 1];
				}
				j++;
			}
		}
		return nom1;
	}

	public static int[] sort(final int[] array3) {
		for (int i = 0; i < array3.length; i++) {
			for (int j = 0; j < array3.length - i - 1; j++) {
				if (array3[j] > array3[j + 1]) {
					int temp = array3[j];
					array3[j] = array3[j + 1];
					array3[j + 1] = temp;
				}
			}
		}
		return array3;
	}

	public static boolean isInt(final double d) {
		return d == (int) d;
	}

	public static String[] split(String s) {
		s = s.replace('f', ' ').trim();
		String[] sp = s.split(",");
		return sp;
	}

	public static boolean compare(final String[] s1, final String s2[]) {
		if (s1.length != s2.length) {
			return false;
		}
		int count = 0;
		for (int i = 0; i < s1.length; i++) {
			for (int j = 0; j < s2.length; j++) {
				if (s2[j].equals(s1[i])) {
					count++;
					break;
				}
			}
		}
		if (count == s1.length) {
			return true;
		}
		return false;
	}

	public static String stringToBinary(int nomofvariable,String s) {
		//s = sub(s);
		String[] part = s.split(",");
		String s1, s2, s4, s5, s6 = "", s7 = "", s3 = "";
		if (part.length == 2) {
			s3 = getx(Integer.parseInt(part[0]), Integer.parseInt(part[1]));
		} else if (part.length == 4) {
			s1 = getx(Integer.parseInt(part[0]), Integer.parseInt(part[1]));
			s2 = getx(Integer.parseInt(part[2]), Integer.parseInt(part[3]));
			s3 = getx2(s1, s2);
		} else if (part.length == 8) {
			s1 = getx(Integer.parseInt(part[0]), Integer.parseInt(part[1]));
			s2 = getx(Integer.parseInt(part[2]), Integer.parseInt(part[3]));
			s4 = getx(Integer.parseInt(part[4]), Integer.parseInt(part[5]));
			s5 = getx(Integer.parseInt(part[6]), Integer.parseInt(part[7]));
			s6 = getx2(s1, s2);
			s7 = getx2(s4, s5);
			s3 = getx2(s6, s7);
		}
		while ( s3.length() <  nomofvariable){
		   	  s3="0"+s3;
			 }
		return s3;
	}

	public static String sub(final String s) {

		String string = "";
		String[] parts = s.split(",");
		int[] newarr = new int[50];
		int[] array = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			array[i] = Integer.parseInt(parts[i]);
		}
		int size = 0;
		newarr[size] = array[0];
		int sum = array[0];
		size++;
		// System.out.println("size = "+array.length);
		for (int i = 1; i < array.length; i++) {
			newarr[size] = array[i] + array[0];
			sum += array[i];
			size++;
		}
		if (array.length == 3 || array.length == 4) {
			newarr[size] = sum;
			size++;
		}
		if (array.length == 4) {
			int f = array[0] + array[array.length - 1];
			for (int i = 1; i < array.length - 1; i++) {
				newarr[size] = array[i] + f;
				size++;
			}
			f = array[0] + array[array.length - 2];
			for (int i = 1; i < array.length - 2; i++) {
				newarr[size] = array[i] + f;
				size++;
			}
		}

		for (int i = 0; i < size; i++) {
			string = string + Integer.toString(newarr[i]) + ",";
		}

		return string;
	}

	public static String getx(final int a, final int b) {

		String s3 = "";

		String s1 = Integer.toBinaryString(a);
		String s2 = Integer.toBinaryString(b);

		while (s1.length() != s2.length()) {

			if (s1.length() < s2.length())
				s1 = "0" + s1;
			else if (s1.length() > s2.length())
				s2 = "0" + s2;

		}
		for (int j = 0; j < s1.length(); j++) {
			if (s1.charAt(j) == s2.charAt(j)) {
				s3 = s3 + s1.charAt(j);
			} else {
				s3 = s3 + "2";
			}
		}
		return s3;
	}

	public static String getx2(String s1, String s2) {

		String s3 = "";

		while (s1.length() != s2.length()) {

			if (s1.length() < s2.length()) {
				s1 = "0" + s1;
			} else if (s1.length() > s2.length()) {
				s2 = "0" + s2;
			}

		}
		for (int j = 0; j < s1.length(); j++) {
			if (s1.charAt(j) == s2.charAt(j)) {
				s3 = s3 + s1.charAt(j);
			} else {
				s3 = s3 + "2";
			}
		}
		return s3;
	}

	public static String[] multiplyingPIs(final String[] petrick) {

		String[] finalPetrick = new String[petrick.length];
		int i = 0;
		String d = "";
		int f = 0;
		while (i < petrick.length) {
			String[] dummy1 = petrick[i].split(",");
			if (i + 1 < petrick.length) {
				String[] dummy2 = petrick[i + 1].split(",");
				d = "";
				for (int j = 0; j < dummy1.length; j++) {
					for (int k = 0; k < dummy2.length; k++) {
						if (containPI(dummy1[j], dummy2[k])) {
							if (!(dummy2[k].length() == 1)) {
								dummy2[k] = removeCommonPI(dummy1[j], dummy2[k]);
								d += dummy1[j] + dummy2[k];
							} else {
								d += dummy1[j];
							}
						} else if (!dummy1[j].equals(dummy2[k])) {
							d += dummy1[j] + dummy2[k];
						}
						d += ",";
					}
				}
				finalPetrick[f++] = d;
				i += 2;
			} else {
				finalPetrick[f++] = petrick[i];
				i += 2;
			}
		}

		if (f == 0) {
			return petrick;
		}

//		String[] finalP = new String[f];
//		for (int l = 0; l < f; l++) {
//			finalP[l] = finalPetrick[l];
//		}

		finalPetrick = Arrays.copyOf(finalPetrick, f);
		if (f == 1) {
			return finalPetrick;
		}
		return multiplyingPIs(finalPetrick);
	}

	public static boolean containPI(final String s1, final String s2) {
		for (int i = 0; i < s1.length(); i++) {
			char x = s1.charAt(i);
			for (int j = 0; j < s2.length(); j++) {
				if (x == s2.charAt(j)) {
					return true;
				}
			}
		}
		return false;
	}

	public static String removeCommonPI(final String s1, String s2) {
		for (int i = 0; i < s1.length(); i++) {
			if (s2.contains(String.valueOf(s1.charAt(i)))) {
				s2 = s2.replaceAll(String.valueOf(s1.charAt(i)), "");
			}
		}
		return s2;
	}

//	public static String[] sorting(final String[] d) {
//		String string;
//		String[] l = new String[d.length];
//		for (int i = 0; i < d.length; i++) {
//			string = d[i];
//			for (int j = 1; j < string.length(); j++) {
//				for (int k = j; k > 0; k--) {
//					if (string.charAt(k) < string.charAt(k - 1)) {
//						string = swap(string, k, k - 1);
//
//					}
//				}
//			}
//			l[i] = string;
//		}
//		return l;
//	}
//
//	public static String swap(String string, final int i, final int j) {
//		char[] c = string.toCharArray();
//		char temp = c[i];
//		c[i] = c[j];
//		c[j] = temp;
//		string = new String(c);
//		return string;
//	}

//	public static String[] minimize(String[] finalPIs) {
//		System.out.println("inside minimize");
//		int newSize = finalPIs.length;
//		for (int i = 0; i < newSize - 1; i++) {
//			for (int j = i + 1; j < newSize; j++) {
//				if (finalPIs[i].length() > finalPIs[j].length()) {
//					if (finalPIs[i].contains(finalPIs[j])) {
//						System.out.println("al awlanya contains al tanya" + i + j);
//						int k = i;
//						while (k < newSize - 1) {
//							finalPIs[k] = finalPIs[k + 1];
//							k++;
//						}
//						newSize--;
//						System.out.println("after shifting");
//					}
//				} else {
//					if (finalPIs[j].contains(finalPIs[i])) {
//						System.out.println("al tanya contains al awlanya");
//						int k = j;
//						while (k < newSize - 1) {
//							finalPIs[k] = finalPIs[k + 1];
//							k++;
//						}
//						newSize--;
//						System.out.println("after shifting");
//					}
//				}
//			}
//		}
//		finalPIs = Arrays.copyOf(finalPIs, newSize);
//		return finalPIs;
//	}

	public static String[] deleteDuplicate(final String[] d) {
		char[] arr1;
		char[] arr2;
		boolean flag = false;
		int size = d.length;
		for (int i = 0; i < d.length; i++) {
			if (d[i].charAt(0) != '5') {
				arr1 = d[i].toCharArray();
				for (int j = i + 1; j < d.length; j++) {
					if (arr1.length <= d[j].length()) {
						// System.out.println("ana gwa");
						for (int k = 0; k < arr1.length; k++) {
							// System.out.println(arr1[k]+"****1"+d[j]);
							if (d[j].contains(Character.toString(arr1[k]))) {
								flag = true;
							} else {
								flag = false;
								break;
							}

						}
						// System.out.println(flag);
						if (flag && d[j].charAt(0) != '5') {
							d[j] = "5" + d[j];
							size--;
						}
					} else if (arr1.length > d[j].length()) {
						arr2 = d[j].toCharArray();
						for (int o = 0; o < arr2.length; o++) {
							// System.out.println(arr2[o]+"****2"+d[i]);

							if (d[i].contains(Character.toString(arr2[o]))) {
								flag = true;
							} else {
								flag = false;
								break;
							}
						}
						// System.out.println(flag);
						if (flag && d[i].charAt(0) != '5') {
							d[i] = "5" + d[i];
							size--;
						}
					}

				}

			}
		}
		String[] l = new String[size];
		int count = 0;
		for (int y = 0; y < d.length; y++) {
			if (d[y].charAt(0) != '5') {
				l[count] = d[y];
				count++;
			}
		}
		return l;
	}

	public static String[] minimumSolutions(String[] finalPIs) {
		int min = finalPIs[0].length();
		for (int i = 0; i < finalPIs.length; i++) {
			if (finalPIs[i].length() < min) {
				min = finalPIs[i].length();
			}
		}
		int size = finalPIs.length;
		for(int i = 0; i <  size; i++) {
			if(finalPIs[i].length() > min) {
				for(int j = i; j < size - 1; j++) {
					finalPIs[j] = finalPIs[j + 1];
				}
				i--;
				size--;
			}
		}
		finalPIs = Arrays.copyOf(finalPIs, size);
		return finalPIs;
	}

	public static String[] getPIs(final String[][][] nom1, final int nomofvariable) {
		String[] primeImplicants = new String[(int) Math.pow(2, nomofvariable)];
		int x = 0;
		for (int i = 0; i < nom1.length; i++) {
			for (int j = 0; j < nom1[0].length; j++) {
				for (int z = 0; z < nom1[0][0].length; z++) {
					if (nom1[i][j][z] != null && nom1[i][j][z].charAt(0) == 'f') {
						primeImplicants[x] = nom1[i][j][z].replaceAll("f", "");
						x++;
					}

				}
			}
		}
		primeImplicants = Arrays.copyOf(primeImplicants, x);
		primeImplicants = removeRepeatedPIs(primeImplicants);
		return primeImplicants;
	}

	public static String[] removeRepeatedPIs(String[] pi) {
		int size = pi.length;
		for(int i = 0; i < size; i++) {
			String[] s1 = pi[i].split(",");
			for(int j = i + 1; j < size; j++) {
				String[] s2 = pi[j].split(",");
				if((s1.length == s2.length) && (s1[0].equals(s2[0]))) {
					if(compare(s1, s2)) {
						for(int k = j ; k < size -1 ; k++) {
							pi[k] = pi[k + 1];
						}
						j--;
						size--;
					}
				}
			}
		}
		pi = Arrays.copyOf(pi, size);
		return pi;
	}

	public static String getTerm(final String string) {
		String string2 = "";
		for(int i = 0; i < string.length(); i++){
			if(string.charAt(i) == '0') {
				string2 += (char) (65 + i) + "'";
			} else if(string.charAt(i) == '1') {
				string2 += (char) (65 + i) ;
			}
		}
		return string2;
	}

	public static String eachPI(final char c, final String[] primeImplicants) {
		int x = c - 'A';
		String s = primeImplicants[x];
		return s;
	}
	public static void printResult(String[][] result,String[] essential,int nomofvariable){
		if(result==null){
			System.out.print("F = ");
			for(int i=0;i<essential.length;i++){
				System.out.print(getTerm(stringToBinary(nomofvariable,essential[i]))+" + ");
			}
		}
		else{
			for(int i=0;i<result.length;i++){
				System.out.print("F"+(i+1)+" = ");
				for(int k=0;k<essential.length;k++){
					System.out.print(getTerm(stringToBinary(nomofvariable,essential[k]))+" + ");
				}
				for (int j=0;j<result[0].length;j++){
					
					System.out.print(result[i][j]+" + ");
				}
				System.out.println();
			}
		}
	}
}